# Certificate signing service

An ACME-like shell server certificate signing service.

A service that listens on a unix domain socket and signs certificate signing requests.
It verifies the CSR's subject corresponds to the username of the peer.
If a user tries to get a certificate for another user the request will be rejected.
It will also add an email address as the SubjectAltName.
This is for client certificates.

## Usage

Run the server:

```shell
$ cert-service-server
```

Get a certificate with the client program:

```shell
$ cert-service-client
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDHPBHCG+BI9EMW
[...]
jV6RSgZDKsR4uB/F5NDy4BA=
-----END PRIVATE KEY-----

-----BEGIN CERTIFICATE-----
MIIDJjCCAg6gAwIBAgIIYW+9Z0sUthMwDQYJKoZIhvcNAQELBQAwFjEUMBIGA1UE
AwwLaGFzaGJhbmcuc2gwHhcNMjAxMjA5MjE0ODI5WhcNMjEwMzA5MjE0ODI5WjAR
MQ8wDQYDVQQDDAZyZXluaXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
AQDHPBHCG+BI9EMWI3lGQLcYB9x8HS8j8GED8kA3JT+Dl7KYSQWgwc8t9TDgOeCi
2rEGTYsEFCBtlIDBmxobBNeJrjPEQzIc4rR9xVUzEx/NRQnVIfmSHVS2KdO5VbuE
qTWW7nMkeT5TyVu1RNy68skwK8wlxIcKPvlc++e63iBKylhQeM46NhPALp1rq+0R
1nOrUTWkS+Am5cqdd/CFd7X39dfSE1KWDfeA+o/+F9pxguvOCxgcbigdbZna++A5
AIOJnovgwyOTCsVLaGk9oAuAgdvA+51kJPXJWfZ4h2NuXCtDgk5Jk2SDZwqmes7V
tLv6R0/nRcXzoBmgpBKJ9JufAgMBAAGjfTB7MB0GA1UdDgQWBBTovATFyp1rnx+n
iw2+N9169G4sEDAhBgNVHREEGjAYgRZyZXluaXJAZGUxLmhhc2hiYW5nLnNoMB8G
A1UdIwQYMBaAFCocYDyF6oJJRo8jBX2523nTtZjQMBYGA1UdJQEB/wQMMAoGCCsG
AQUFBwMCMA0GCSqGSIb3DQEBCwUAA4IBAQAwEuoaUFEq8o6RQCGJNpExiKT1Yz5T
/0PMi0XmXfdPe8bzo3b6JfNZ7S7tgKlYdRXUxPx8Yjed9ZzeZejk01/WsJGOE2Hl
WDuKGmS4cB09/5one+QXg1A8dWfnteVFem1atyOsXQHYHaM2UEItxjK/Axw0MgMx
tKKTux6qrZhPnnjFjaYHFX7etVAFD8ePaX/kgLkXGKAaKAWJ2rQU3eZe/yCMG6kl
D036TTu0FjmzRS86WyDqYwJJDiwu4LERscMn6sQlCbX3xj63l19deAf908k78mF2
hc+6DdQZU1foRfjsBjlaF44ggx+6/pGcr0rByvX5gKorVg1BsUcZpXZZ
-----END CERTIFICATE-----
```

A key and CSR is created by the client and the key and certificate is just printed out to stdout in PEM format.
Ideally, you should be able to pass a CSR you have created on another machine, but it is not yet implemented in this proof of concept.

## Motivation

This was written with [hashbang.sh](https://hashbang.sh/) in mind - a open registration shell server.
SSH keys are used for authenticating with hashbang.sh machines, but users might want to connect to services using TLS or send signed and/or encrypted email.

## Design

The server `cert-service-server` loads up a certificate authority and listens on a socket.
A client `cert-service-client` can connect to this socket and make a signing request.
Using `SO_PEERCRED` the server decides if the client is allowed to have its certificate signed.

The peers communicate using a custom protocol based on [ASN.1](https://en.wikipedia.org/wiki/ASN.1).

**This is a proof of concept put together in an evening.
The quality of the code reflects that.**
