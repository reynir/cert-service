open Lwt.Syntax

let sock_path = "/home/reynir/cert.sock"
let version = `V1

let organization = "HashBang"

let csr user =
  let dn = 
    X509.Distinguished_name.(
      Relative_distinguished_name.singleton (CN user)
      |> Relative_distinguished_name.add (O organization)) in
  let key : X509.Private_key.t =
    `RSA (Mirage_crypto_pk.Rsa.generate ~bits:2048 ()) in
  let csr = X509.Signing_request.create [dn] key in
  key, csr


let main () =
  Mirage_crypto_rng_lwt.initialize ();
  let user = Unix.getpwuid (Unix.getuid ()) in
  let fd = Lwt_unix.socket Unix.PF_UNIX Unix.SOCK_STREAM 0 in
  let* () = Lwt_unix.connect fd (Unix.ADDR_UNIX sock_path) in
  let key, csr = csr user.pw_name in
  let csr =
    Result.fold csr ~ok:Fun.id
      ~error:(fun (`Msg e) -> Fmt.kstr failwith "CSR error: %s" e)
  in
  let* r = Cert_service.Wire_lwt.write_wire fd
      ({ version }, `Command (`Sign_request csr)) in
  let* () =
    match r with
    |Error `Exception ->
      let* () = Lwt_io.eprintl "Error writing to cert service" in
      exit 2
    | Ok () ->
      Lwt.return_unit
  in
  let* r = Cert_service.Wire_lwt.read_wire fd in
  let* cert =
    match r with
    | Ok ({ version = `V1 }, `Sign cert) ->
      Lwt.return cert
    | Ok ({ version = `V1 }, `Failure e) ->
      let* () = Lwt_io.eprintf "cert service reported failure: %s\n" e in
      exit 1
    | Ok ({ version = `V1 }, `Command _) | Error _ ->
      let* () = Lwt_io.eprintl "Error reading from cert service" in
      exit 2
  in
  let* () = Lwt_io.printl (Cstruct.to_string (X509.Private_key.encode_pem key)) in
  Lwt_io.printl (Cstruct.to_string (X509.Certificate.encode_pem cert))

let () = Lwt_main.run (main ())
