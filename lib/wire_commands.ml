type version = [ `V1 ]

let pp_version ppf v =
  Fmt.int ppf (match v with `V1 -> 1)

type header = {
  version : version;
}

type t = [
  | `Sign_request of X509.Signing_request.t
]

type res = [
  | `Command of t
  | `Sign of X509.Certificate.t
  | `Failure of string
]

type wire = header * res
